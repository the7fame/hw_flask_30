from flask_sqlalchemy.model import DefaultMeta
from sqlalchemy import (
    Boolean,
    Column,
    DateTime,
    ForeignKey,
    Integer,
    String,
    UniqueConstraint,
    func,
)
from sqlalchemy.exc import NoResultFound

from app.flask_app import db

BaseModel: DefaultMeta = db.Model


class Client(BaseModel):
    __tablename__ = "clients"

    id = Column(Integer, primary_key=True, nullable=False)
    name = Column(String(50), nullable=False)
    surname = Column(String(50), nullable=False)
    credit_card = Column(String(50))
    car_number = Column(String(10))

    def to_json(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    @classmethod
    def exist_user(cls, client_id):
        try:
            user = db.session.query(Client).where(Client.id == client_id).first()
            return user
        except NoResultFound:
            print("No such user")


class Parking(BaseModel):
    __tablename__ = "parkings"

    id = Column(Integer, primary_key=True, nullable=False)
    address = Column(String(100), nullable=False)
    opened = Column(Boolean)
    count_places = Column(Integer, nullable=False)
    count_available_places = Column(Integer, nullable=False)

    @classmethod
    def exist_parking(cls, parking_id):
        try:
            user = db.session.query(Parking).where(Parking.id == parking_id).first()
            return user
        except NoResultFound:
            print("No such parking")

    @classmethod
    def increase_places(cls, parking_id):
        increase = (
            db.session.query(Parking)
            .where(Parking.id == parking_id)
            .update({"count_available_places": Parking.count_available_places + 1})
        )
        return increase

    @classmethod
    def decrease_places(cls, parking_id):
        decrease = (
            db.session.query(Parking)
            .where(Parking.id == parking_id)
            .update({"count_available_places": Parking.count_available_places - 1})
        )
        return decrease

    def to_json(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Client_parking(BaseModel):
    __tablename__ = "client_parkings"

    id = Column(Integer, primary_key=True, nullable=False)
    client_id = Column(Integer, ForeignKey("clients.id"))
    parking_id = Column(Integer, ForeignKey("parkings.id"))
    time_in = Column(DateTime, default=func.now())
    time_out = Column(DateTime)
    __table_args__ = (
        UniqueConstraint("client_id", "parking_id", name="unique_client_parking"),
    )

    def to_json(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    @classmethod
    def exist_client_parking(cls, client_id, parking_id):
        try:
            client_parking = db.session.query(Client_parking).where(
                Client_parking.client_id == client_id,
                Client_parking.parking_id == parking_id,
            )
            return client_parking
        except NoResultFound:
            print("No such user with such parking")
