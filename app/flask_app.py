from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import func

db = SQLAlchemy()


def create_app():
    app = Flask(__name__)
    db.init_app(app)
    from app.models import Client, Client_parking, Parking

    @app.route("/", methods=["GET"])
    def welcome_page():
        return {"message": "welcome"}, 200

    @app.route("/clients", methods=["GET"])
    def get_all_users():
        all_users = db.session.query(Client).all()
        if all_users:
            return {"message": [user.to_json() for user in all_users]}
        return {"message": "no any user"}, 200

    @app.route("/clients/<int:client_id>", methods=["GET"])
    def get_user_by_id(client_id):
        user_by_id = Client.exist_user(client_id)
        if user_by_id:
            return {"message": user_by_id.to_json()}, 200
        return {"message": "check parameters"}, 404

    @app.route("/clients", methods=["POST"])
    def create_new_client():
        name = request.form.get("name", type=str)
        surname = request.form.get("surname", type=str)
        credit_card = request.form.get("credit_card", type=str)
        car_number = request.form.get("car_number", type=str)
        create_user = Client(
            name=name,
            surname=surname,
            credit_card=credit_card,
            car_number=car_number,
        )
        db.session.add(create_user)
        db.session.commit()
        return {"message": "success"}, 201

    @app.route("/parkings", methods=["POST"])
    def create_new_parking():
        address = request.form.get("address", type=str)
        opened = request.form.get("opened", type=bool)
        count_places = request.form.get("places", type=int)
        count_available_places = count_places
        create_parking = Parking(
            address=address,
            opened=opened,
            count_places=count_places,
            count_available_places=count_available_places,
        )
        db.session.add(create_parking)
        db.session.commit()
        return {"message": "success"}, 201

    @app.route("/client_parkings", methods=["POST"])
    def user_ride_in_parking():
        client_id_ride_in = request.form.get("client_id", type=int)
        parking_id_ride_in = request.form.get("parking_id", type=int)
        client_in = Client.exist_user(client_id_ride_in)
        parking = Parking.exist_parking(parking_id_ride_in)
        if client_in and parking:
            if parking.opened:
                write_to_db = Client_parking(
                    client_id=client_id_ride_in, parking_id=parking_id_ride_in
                )
                db.session.add(write_to_db)
                Parking.decrease_places(parking_id_ride_in)
                db.session.commit()
                return {"message": "thanks for the trust"}, 201
            else:
                return {"message": "sorry, we'are closed"}
        return {"message": "check parameters"}, 404

    @app.route("/clients_out", methods=["POST"])
    def user_ride_out_from_the_parking():
        client_id_ride_out = request.form.get("client_id", type=int)
        parking_id_ride_out = request.form.get("parking_id", type=int)
        client_parking_check = Client_parking.exist_client_parking(
            client_id_ride_out, parking_id_ride_out
        )
        if client_parking_check:
            check_card = Client.exist_user(client_id=client_id_ride_out)
            if check_card.credit_card:
                db.session.query(Client_parking).where(
                    Client_parking.client_id == client_id_ride_out,
                    Client_parking.parking_id == parking_id_ride_out,
                ).update({Client_parking.time_out: func.now()})
                Parking.increase_places(parking_id_ride_out)
                db.session.commit()
                return {"message": "thanks"}, 201
            else:
                return {"message": "please, pay for our services"}, 404
        return {"message": "check parameters"}

    return app
