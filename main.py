from app.create_db import create_database
from app.flask_app import create_app, db

if __name__ == "__main__":
    app = create_app()
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///hw.db"
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    create_database(db, app)
    app.run()
