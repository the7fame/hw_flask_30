import pytest

from app.flask_app import create_app
from app.flask_app import db as _db
from app.models import Client, Client_parking, Parking


@pytest.fixture
def app():
    _app = create_app()
    _app.config["TESTING"] = True
    _app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite://"

    with _app.app_context():
        _db.create_all()
        user_with_card = Client(
            name="name1",
            surname="surname1",
            credit_card="creditcard",
            car_number="1234AH",
        )
        user_without_card = Client(
            name="name2", surname="surname2", car_number="1234AH"
        )
        parking_open = Parking(
            address="some address",
            opened=True,
            count_places=100,
            count_available_places=100,
            id=1,
        )
        parking_close = Parking(
            address="some address",
            opened=False,
            count_places=100,
            count_available_places=100,
            id=2,
        )
        user_in_parking = Client_parking(client_id=1, parking_id=1)
        _db.session.add_all([user_with_card, user_without_card])
        _db.session.add_all([parking_open, parking_close])
        _db.session.add(user_in_parking)
        _db.session.commit()

        yield _app
        _db.session.close()
        _db.drop_all()


@pytest.fixture
def client(app):
    client = app.test_client()
    yield client


@pytest.fixture
def db(app):
    with app.app_context():
        yield _db
