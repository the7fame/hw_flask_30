import pytest

from app.models import Client, Parking
from tests.factories import ClientFactory, ParkingFactory


@pytest.mark.parametrize(
    "class_create, model_db", [(ClientFactory, Client), (ParkingFactory, Parking)]
)
def test_create_user(app, db, class_create, model_db):
    create = class_create()
    db.session.commit()
    assert create.id is not None
    assert len(db.session.query(model_db).all()) == 3
