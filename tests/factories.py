import random

import factory

from app.flask_app import db
from app.models import Client, Parking


class ClientFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Client
        sqlalchemy_session = db.session

    name = factory.Faker("first_name")
    surname = factory.Faker("last_name")
    credit_card = factory.LazyAttribute(lambda x: "%s 1111" % random.randrange(1, 1000))
    car_number = factory.LazyAttribute(lambda x: "%s AHH" % random.randrange(1, 10000))


class ParkingFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Parking
        sqlalchemy_session = db.session

    address = factory.LazyAttribute(
        lambda x: "%s Address" % random.choice(["some address", "address some"])
    )
    opened = factory.LazyAttribute(lambda x: random.randint(0, 1))
    count_places = factory.LazyAttribute(lambda x: random.randrange(0, 1000))
    count_available_places = count_places
