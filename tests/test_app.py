import datetime

import pytest

from app.models import Client, Client_parking, Parking


def test_user(client):
    response = client.get("/clients/1")
    assert response.status_code == 200
    assert response.json["message"] == {
        "id": 1,
        "name": "name1",
        "surname": "surname1",
        "credit_card": "creditcard",
        "car_number": "1234AH",
    }


def test_create_user(client, db):
    data = {
        "name": "name2",
        "surname": "surname2",
        "credit_card": "creditcatd2",
        "car_number": "1235AH",
    }
    response = client.post("/clients", data=data)
    info_client = (
        db.session.query(Client)
        .where(
            Client.name == data.get("name"),
            Client.surname == data.get("surname"),
            Client.credit_card == data.get("credit_card"),
            Client.car_number == data.get("car_number"),
        )
        .first()
    )
    assert info_client.to_json()["name"] == data["name"]
    assert info_client.to_json()["surname"] == data["surname"]
    assert info_client.to_json()["credit_card"] == data["credit_card"]
    assert info_client.to_json()["car_number"] == data["car_number"]
    assert response.status_code == 201


def test_create_parking(client, db):
    data = {"address": "some address", "opened": True, "places": 100}
    response = client.post("/parkings", data=data)
    info_parking = (
        db.session.query(Parking)
        .where(
            Parking.address == data.get("address"),
            Parking.opened == data.get("opened"),
            Parking.count_places == data.get("places"),
            Parking.count_available_places == data.get("places"),
        )
        .first()
    )
    assert info_parking.to_json()["address"] == data["address"]
    assert info_parking.to_json()["opened"] == data["opened"]
    assert info_parking.to_json()["count_places"] == data["places"]
    assert info_parking.to_json()["count_available_places"] == data["places"]
    assert response.status_code == 201


def test_app_config(app):
    assert not app.config["DEBUG"]
    assert app.config["TESTING"]
    assert app.config["SQLALCHEMY_DATABASE_URI"] == "sqlite://"


@pytest.mark.parametrize("route", ["/", "/clients", "/clients/1"])
def test_route_status(client, route):
    rv = client.get(route)
    assert rv.status_code == 200


@pytest.mark.parking
def test_car_move_in_closed_parking(client):
    data = {"client_id": 2, "parking_id": 2}
    response = client.post("/client_parkings", data=data)
    assert response.json["message"] == "sorry, we'are closed"


@pytest.mark.parking
def test_car_move_in_parking(client, db):
    data = {"client_id": 2, "parking_id": 1}
    response = client.post("/client_parkings", data=data)
    assert response.status_code == 201
    check_info_in_parking = (
        db.session.query(Client_parking)
        .where(
            Client_parking.client_id == data.get("client_id"),
            Client_parking.parking_id == data.get("parking_id"),
        )
        .first()
    )
    assert check_info_in_parking.client_id == data["client_id"]
    assert check_info_in_parking.parking_id == data["parking_id"]
    assert check_info_in_parking.time_in == datetime.datetime.utcnow().replace(
        microsecond=0
    )
    assert check_info_in_parking.time_out != datetime.datetime.utcnow().replace(
        microsecond=0
    )


@pytest.mark.parking
def test_car_in_not_found(client):
    data = {"client_id": "one", "parking_id": "one"}
    response = client.post("/client_parkings", data=data)
    assert response.status_code != 201
    assert response.json["message"] == "check parameters"


@pytest.mark.parking
def test_car_move_out_parking(client, db):
    data = {"client_id": 1, "parking_id": 1}
    response = client.post("/clients_out", data=data)
    assert response.status_code == 201
    info_about_car_out = (
        db.session.query(Client_parking)
        .where(
            Client_parking.client_id == data.get("client_id"),
            Client_parking.parking_id == data.get("parking_id"),
        )
        .first()
    )
    assert info_about_car_out.time_out == datetime.datetime.utcnow().replace(
        microsecond=0
    )


@pytest.mark.parking
def test_car_out_without_credit_card(client, db):
    data = {"client_id": 2, "parking_id": 1}
    response_1 = client.post("/client_parkings", data=data)
    assert response_1.status_code == 201
    response_2 = client.post("/clients_out", data=data)
    assert response_2.status_code == 404
    info_about_car_in = (
        db.session.query(Client_parking)
        .where(
            Client_parking.client_id == data.get("client_id"),
            Client_parking.parking_id == data.get("parking_id"),
        )
        .first()
    )
    assert info_about_car_in.time_out != datetime.datetime.utcnow().replace(
        microsecond=0
    )
